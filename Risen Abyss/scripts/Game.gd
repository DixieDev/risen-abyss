extends Node2D

# Knower of everything about the game

signal on_all_actions_confirmed
signal on_all_turns_complete

var peer_locations = {}

var local_actions = null
var peer_actions = []
var completed_turns = 0

func set_local_actions(steps, actions):
	local_actions = make_action(-1, steps, actions)
	check_all_actions_confirmed()

func add_peer_actions(id, steps, actions):
	var added = false
	for i in range(peer_actions.size()):
		if peer_actions[i].id == id:
			peer_actions[i] = make_action(id, steps, actions)
			added = true
			printerr("Received actions multiple times for peer " + str(id))
			break
	
	if not added:
		peer_actions.append(make_action(id, steps, actions))
	
	check_all_actions_confirmed()

func check_all_actions_confirmed():
	if local_actions != null and peer_actions.size() == MpMan.peers.size():
		completed_turns = 0
		emit_signal("on_all_actions_confirmed")

func erase_action_for_peer(id):
	for i in range(peer_actions.size()):
		if peer_actions[i].id == id:
			peer_actions.remove(i)

func clear_actions():
	local_actions = null
	peer_actions = []

func make_action(id, steps, actions):
	var new_actions = []
	var new_steps = []
	for step in steps: new_steps.append(step)
	for action in actions: new_actions.append(action)
	return {
		"id": id,
		"steps": new_steps,
		"actions": new_actions,
	}

func report_turn_completion():
	rpc("rpc_report_turn_completion")

remotesync func rpc_report_turn_completion():
	completed_turns += 1
	if completed_turns == 1 + MpMan.peers.size():
		clear_actions()
		emit_signal("on_all_turns_complete")
