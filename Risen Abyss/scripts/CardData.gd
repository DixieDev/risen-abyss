extends Node2D

onready var cards = [
	{
		"idx": 0,
		"name": "Forage",
		"description": "Search your surroundings for equipment.",
	},
	{
		"idx": 1,
		"name": "Lift",
		"description": "Lift an enemy overhead, immobilizing them. This card transforms if you are already lifting an enemy.",
	},
	{
		"idx": 2,
		"name": "Throw",
		"description": "Throw your held enemy, inflicting heavy damage."
	},
	{
		"idx": 3,
		"name": "Bite",
		"description": "Gnash at a nearby enemy, inflicting moderate damage."
	},
	{
		"idx": 4,
		"name": "Poisonous Bite",
		"description": "Sink your teeth into an enemy's flesh, inflicting light damage and poisoining them."
	},
]

func get_card_by_name(card_name: String):
	for card in cards:
		if card.name == card_name:
			return card
	printerr("Could not find card with name " + card_name)
	return null
