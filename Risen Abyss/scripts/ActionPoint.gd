extends Control

onready var normal = load("res://art/ui/action_point.tres")
onready var broken = load("res://art/ui/action_point_broken.tres")
onready var usages = {
	"move": load("res://art/ui/action_move.tres"),
	"interact": load("res://art/ui/action_interact.tres"),
	"defend": load("res://art/ui/action_defend.tres"),
	"rest": load("res://art/ui/action_rest.tres"),
}

var is_broken = false

func _ready():
	var element = MpMan.local_player.element
	$inner.modulate = Color(GameConstants.elements[element].colour_code)

func restore_gem():
	is_broken = false
	$inner.texture = normal
	$usage.visible = false

func break_gem(usage):
	is_broken = true
	$inner.texture = broken
	$usage.visible = true
	$usage.texture = usages[usage]
