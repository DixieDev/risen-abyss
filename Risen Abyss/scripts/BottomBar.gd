extends Control

onready var action_point = load("res://scenes/ActionPoint.tscn")
onready var action_points = get_node("Portrait/ActionPoints")
onready var player_controller = get_node("../../LocalController")
onready var player_cam = get_node("../../PlayerCam")
onready var action_interact = get_node("Actions/Interact")
onready var action_rest = get_node("Actions/Rest")
onready var action_defend = get_node("Actions/Defend")

var mouse_over_portrait = false

func _ready():
	var char_name = MpMan.local_player.character
	var element = MpMan.local_player.element
	var element_colour = GameConstants.elements[element].colour_code
	var bbtext = "[center]" + char_name+ " of [color=" + element_colour+ "]" + element + "[/color][/center]"
	$Portrait/Nameplate/Name.bbcode_enabled = true
	$Portrait/Nameplate/Name.bbcode_text = bbtext
	$Portrait/Image.texture = GameConstants.characters[char_name].texture
	$Portrait/Image.modulate = Color(element_colour)
	for i in range(player_controller.max_action_points):
		var point: Control = action_point.instance()
		point.rect_position.x = 9
		point.rect_position.y = 9 + 40 * i
		action_points.add_child(point)

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == 1 and event.pressed:
			if mouse_over_portrait:
				 player_cam.follow_player()

func set_action_info(info: Array, is_full: bool):
	if is_full:
		disable_action_buttons()
	else:
		enable_action_buttons()
	
	for child in action_points.get_children():
		if info.size() == 0:
			child.restore_gem()
		else:
			child.break_gem(info[0])
			info.remove(0)

func restore_all_actions():
	enable_action_buttons()
	for point in action_points.get_children():
		point.restore_gem()

func disable_action_buttons():
	action_interact.disabled = true
	action_rest.disabled = true
	action_defend.disabled = true

func enable_action_buttons():
	action_interact.disabled = false
	action_rest.disabled = false
	action_defend.disabled = false


func on_mouse_entered_portrait():
	mouse_over_portrait = true

func on_mouse_exited_portrait():
	mouse_over_portrait = false
