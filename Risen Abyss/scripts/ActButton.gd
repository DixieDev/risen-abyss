extends Button

func _ready():
	Game.connect("on_all_turns_complete", self, "on_all_turns_complete")

func on_turn_submitted():
	disabled = true

func on_all_turns_complete():
	disabled = false
