extends Control

var id = -1
var is_local = false

signal ready_changed(pressed)

func set_text(text):
	$Username.text = text

func set_local(local):
	is_local = local
	if is_local:
		$Username.modulate = Color(0.0, 1.0, 0.0, 1.0)
	else:
		$Username.modulate = Color(1.0, 1.0, 1.0, 1.0)

func is_ready():
	return $Ready.pressed

func set_ready(pressed):
	$Ready.pressed = pressed

func on_local_toggle(pressed):
	emit_signal("ready_changed", pressed)
