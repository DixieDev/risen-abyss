extends Node2D

signal peer_connected
signal peer_disconnected
signal connected_to_server
signal connection_failed
signal server_disconnected

var local_player
var peers = {}

func _ready():
	var _e
	_e = $Timeout.connect("timeout", self, "on_connection_failed")	
	_e = get_tree().connect("network_peer_connected", self, "on_peer_connected")
	_e = get_tree().connect("network_peer_disconnected", self, "on_peer_disconnected")
	_e = get_tree().connect("connected_to_server", self, "on_connected_to_server")
	_e = get_tree().connect("connection_failed", self, "on_connection_failed")
	_e = get_tree().connect("server_disconnected", self, "on_server_disconnected")

func is_host():
	return get_tree().is_network_server()

func disallow_new_connections():
	get_tree().set_refuse_new_network_connections(true)


# Player Info funcs

func make_local_player_info(username):
	return {
		"id": -1,
		"username": username,
		"element": "Void",
		"character": "None",
	}

func make_peer_info(id, username):
	return {
		"id": id,
		"username": username,
		"element": "Void",
		"character": "None",
	}


# Init functions

func start_host(username, port):
	peers.clear()
	local_player = make_local_player_info(username)
	var peer = NetworkedMultiplayerENet.new()
	var result = peer.create_server(int(port), 8)
	get_tree().network_peer = peer
	return result

func start_client(username, ip, port):
	peers.clear()
	local_player = make_local_player_info(username)
	var peer = NetworkedMultiplayerENet.new()
	if (peer.create_client(ip, int(port)) == OK):
		get_tree().network_peer = peer
	else:
		on_connection_failed()


# New peer flow

func on_peer_connected(id):
	rpc_id(id, "register_player", local_player)

func on_peer_disconnected(id):
	peers.erase(id)
	emit_signal("peer_disconnected", id)

remote func register_player(player_info):
	var id = get_tree().get_rpc_sender_id()
	var peer_info = make_peer_info(id, player_info.username)
	peers[id] = peer_info
	emit_signal("peer_connected", peer_info)


# Connection to server flow

func on_connected_to_server():
	print("Connected to server")
	$Timeout.start()
	rpc("ask_for_approval")

remote func ask_for_approval():
	if is_host():
		var id = get_tree().get_rpc_sender_id()
		rpc_id(id, "confirm_connection")

remote func confirm_connection():
	emit_signal("connected_to_server")
	$Timeout.stop()

func on_connection_failed():
	emit_signal("connection_failed")

func on_server_disconnected():
	emit_signal("server_disconnected")
