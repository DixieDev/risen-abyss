extends Control

onready var username = get_node("Username")
onready var ip = get_node("Host IP")
onready var port = get_node("Port")

func _ready():
	if OS.has_feature("editor"):
		username.text = "Debug Man"
		$New.visible = true
	else:
		$New.visible = false
	MpMan.connect("connected_to_server", self, "on_connected")
	MpMan.connect("connection_failed", self, "on_connection_failed")

func on_host_clicked():
	set_buttons_visible(false)
	var name_text = username.text.replace(" ", "");
	if name_text.length() == 0:
		on_no_username()
	elif (MpMan.start_host(name_text, port.text) == OK):
		goto_next_scene()
	else:
		on_hosting_failed()

func on_new_instance_clicked():
	OS.execute("godot", ["."], false)

func on_join_clicked():
	set_buttons_visible(false)
	var name_text = username.text.replace(" ", "")
	if name_text.length() == 0:
		on_no_username()
	else:
		MpMan.start_client(name_text, ip.text, port.text)

func on_connected():
	goto_next_scene()

func on_connection_failed():
	$ErrorPopup.dialog_text = "Failed to connect to host"
	$ErrorPopup.popup_centered()
	set_buttons_visible(true)

func on_hosting_failed():
	$ErrorPopup.dialog_text = "Failed to host game"
	$ErrorPopup.popup_centered()
	set_buttons_visible(true)

func on_no_username():
	$ErrorPopup.dialog_text = "Please enter a proper username"
	$ErrorPopup.popup_centered()
	set_buttons_visible(true)

func goto_next_scene():
	get_tree().change_scene("res://scenes/Lobby.tscn")

func set_buttons_visible(visibility):
	$Host.visible = visibility
	$Join.visible = visibility
