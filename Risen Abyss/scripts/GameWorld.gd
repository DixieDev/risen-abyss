extends Node2D

const STATE_MISC = 0;
const STATE_PLAYER_INPUT = 1;
const STATE_EXECUTING_PLAYER_TURNS = 2;
const STATE_AI_DECISIONS = 3;
const STATE_EXECUTING_AI_TURNS = 4;
const STATE_SYNCING = 5;

onready var character = load("res://scenes/Character.tscn")
onready var local_controller = get_node("LocalController")
onready var players = get_node("Players")
onready var entities = get_node("Entities")
onready var cam = get_node("PlayerCam")
onready var map: TileMap = get_node("TileMap")
var state = STATE_PLAYER_INPUT
var execution_ready = true
var steps_in_progress = 0
var step_delay = 0.2
var players_finished_acting = false

func _ready():
	# Spawn players
	var local_char = character.instance()
	local_char.position = get_spawn_by_element(MpMan.local_player.element).position
	local_char.initialise(-1, MpMan.local_player.character, MpMan.local_player.element)
	local_char.is_local = true
	local_char.connect("travel_complete", self, "on_player_travel_complete")
	players.add_child(local_char)
	cam.assign_target(local_char)
	local_controller.assign_pawn(local_char)
	local_controller.enable_control()
	
	for peer in MpMan.peers.values():
		var peer_char = character.instance()
		peer_char.position = get_spawn_by_element(peer.element).position
		peer_char.initialise(peer.id, peer.character, peer.element)
		peer_char.connect("travel_complete", self, "on_player_travel_complete")
		players.add_child(peer_char)
	
	Game.connect("on_all_turns_complete", self, "on_all_turns_complete")
	Game.connect("on_all_actions_confirmed", self, "on_all_actions_confirmed")

func _process(_delta):
	if state == STATE_EXECUTING_PLAYER_TURNS:
		if execution_ready:
			# Initiate each player's action, and record whether any are actually doing anything
			players_finished_acting = true
			if execute_next_player_action(Game.local_actions):
				players_finished_acting = false
			for peer_actions in Game.peer_actions:
				if execute_next_player_action(peer_actions):
					players_finished_acting = false
			
			# If any players' might collide, tell them to bounce back and stop
			var chars = players.get_children()
			for a in chars:
				for b in chars:
					if a.player_id != b.player_id:
						if a.target_pos == b.target_pos:
							a.cancel_halfway = true
							b.cancel_halfway = true
							clear_actions_for_player(a.player_id)
							clear_actions_for_player(b.player_id)
							break
						elif a.target_pos == b.start_pos:
							a.cancel_halfway = true
							clear_actions_for_player(a.player_id)
							break
			
			execution_ready = false
		elif steps_in_progress == 0:
			if players_finished_acting:
				state = STATE_SYNCING
				Game.report_turn_completion()
			elif $ExecutionTimer.time_left == 0:
				$ExecutionTimer.start(step_delay)

func clear_actions_for_player(id):
	if id == -1:
		Game.local_actions = null
	else:
		for actions in Game.peer_actions:
			if actions.id == id:
				Game.peer_actions.erase(actions)
				break

func execute_next_player_action(actions):
	if actions == null:
		return false
	
	if actions.steps.size() > 0:
		for player in players.get_children():
			if player.player_id == actions.id:
				player.set_target(player.position + actions.steps[0])
				player.cancel_halfway = false
				actions.steps.remove(0)
				steps_in_progress += 1
				return true
	else:
		# TODO: Other actions than movement
		pass
	
	return false

func get_spawn_by_element(element):
	for spawn in $Spawns.get_children():
		if spawn.name == element:
			return spawn
	printerr("No spawn found for element "+ element)
	return null

func pos_to_cell(pos: Vector2) -> Vector2:
	var x = round(pos.x / map.cell_size.x)
	var y = round(pos.y / map.cell_size.y)
	return Vector2(x, y)

func is_position_free(pos):
	var cell = pos_to_cell(pos)
	
	var offset_cell = pos_to_cell(pos - map.cell_size/2)
	if map.get_cell(offset_cell.x, offset_cell.y) >= 2:
		return false
	
	for entity in entities.get_children():
		var entity_cell = pos_to_cell(entity.position)
		if cell == entity_cell:
			return false
	
	return true

func on_all_actions_confirmed():
	local_controller.disable_control()
	state = STATE_EXECUTING_PLAYER_TURNS
	execution_ready = true

func on_execution_timer_complete():
	execution_ready = true

func on_player_travel_complete(_character):
	steps_in_progress -= 1

func on_all_turns_complete():
	state = STATE_PLAYER_INPUT
	local_controller.enable_control()
