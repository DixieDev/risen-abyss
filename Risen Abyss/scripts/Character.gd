class_name Character
extends Sprite

signal travel_complete(character)

const width = 60
const height = 60

var character = "None"
var element = "Void"
var player_id
var character_texture
var is_local = false

var start_pos = Vector2(0.0, 0.0)
var target_pos = Vector2(0.0, 0.0)
var travel_time = 0.0
var travel_duration = 0.3
var cancel_halfway = false

var executing = false
var execution = {
	"steps": [],
	"actions": [],
}

func initialise(id, character_name, element_name):
	player_id = id
	character = character_name
	element = element_name
	frame = GameConstants.characters[character_name].frame
	modulate = Color(GameConstants.elements[element_name].colour_code)
	character_texture = GameConstants.characters[character_name].texture
	start_pos = position
	target_pos = position

func _process(delta):
	if position != target_pos:
		var travel_completed = false
		travel_time += delta
		
		# Handle cancelling a journey halfway
		if cancel_halfway and travel_time > travel_duration/2.0:
			var tmp = target_pos
			target_pos = start_pos
			start_pos = tmp
			cancel_halfway = false
		
		# Actually handle moving the character
		if travel_time >= travel_duration:
			# Travel is completed in this case. All values are clamped
			travel_time = travel_duration
			travel_completed = true
			position = target_pos
		else:
			# Normal travelling, formula just lerps us to the target
			var target_disp = target_pos - start_pos
			position = start_pos + target_disp * (travel_time / travel_duration)
		
		# If we finished travelling then let listeners know
		if travel_completed:
			emit_signal("travel_complete", self)

func set_target(target):
	start_pos = position
	travel_time = 0.0
	target_pos = target

func is_mouse_hovered():
	var mouse = get_global_mouse_position()
# warning-ignore:integer_division
# warning-ignore:integer_division
	var bounds = Rect2(position - Vector2(width/2, height/2), Vector2(width, height))
	var in_x = mouse.x >= bounds.position.x and mouse.x <= bounds.position.x+bounds.size.x
	var in_y = mouse.y >= bounds.position.y and mouse.y <= bounds.position.y+bounds.size.y
	return in_x and in_y
