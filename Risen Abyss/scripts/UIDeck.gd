extends Panel

func _ready():
	var element = MpMan.local_player.element
	var col = Color(GameConstants.elements[element].colour_code)
	$Border.modulate = col
	match element:
		"Creation": $Back.texture = load("res://art/ui/card_back_creation.tres")
		"Void": $Back.texture = load("res://art/ui/card_back_void.tres")
		"Light": $Back.texture = load("res://art/ui/card_back_light.tres")
		"Darkness": $Back.texture = load("res://art/ui/card_back_darkness.tres")
		"Fire": $Back.texture = load("res://art/ui/card_back_fire.tres")
		"Water": $Back.texture = load("res://art/ui/card_back_water.tres")
		"Earth": $Back.texture = load("res://art/ui/card_back_earth.tres")
		"Air": $Back.texture = load("res://art/ui/card_back_air.tres")
	$Back.modulate = col
