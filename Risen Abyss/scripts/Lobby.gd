extends Control

onready var list = get_node("PlayerList")
onready var lobby_player = load("res://scenes/LobbyPlayer.tscn")
onready var begin = get_node("Begin")

var unassigned_elements = [
	"Creation",
	"Void",
	"Light",
	"Darkness",
	"Fire",
	"Water",
	"Earth",
	"Air",
]

func _ready():
	MpMan.connect("server_disconnected", self, "on_server_disconnected")
	MpMan.connect("peer_connected", self, "add_entry")	
	MpMan.connect("peer_disconnected", self, "remove_entry")
	begin.disabled = true
	if MpMan.is_host():
		begin.visible = true
	else:
		begin.visible = false
	add_entry(MpMan.local_player, true)
	for peer in MpMan.peers.values():
		add_entry(peer)

func try_enable_begin():
	var locked_in = $CharacterSelect.get_locked_in_count()
	var total = 1+MpMan.peers.size()
	var threshold = 1
	
	if OS.has_feature("editor"):
		threshold = 0
	
	if total > threshold and locked_in == total:
		begin.disabled = false
	else:
		begin.disabled = true

func add_entry(info, is_local = false):
	var entry = lobby_player.instance()
	if is_local:
		entry.connect("ready_changed", self, "on_local_ready_changed")
	entry.id = info.id
	entry.set_local(is_local)
	entry.set_text(info.username)
	list.add_child(entry)
	align_entries()
	try_enable_begin()

func remove_entry(id):
	for child in list.get_children():
		if child.id == id:
			list.remove_child(child)
	align_entries()
	try_enable_begin()

func align_entries():
	var children = list.get_children()
	for i in range(children.size()):
		var cursor = i * 74
		children[i].rect_position.y = cursor

func on_server_disconnected():
	$ErrorPopup.dialog_text = "Lost connection to host"
	$ErrorPopup.popup_centered()
	$ErrorPopup.connect("popup_hide", self, "return_to_main_menu")

func return_to_main_menu():
	get_tree().change_scene("res://scenes/MainMenu.tscn")

func on_local_ready_changed(pressed):
	try_enable_begin()
	rpc("on_remote_ready_changed", pressed)

remote func on_remote_ready_changed(pressed):
	var id = get_tree().get_rpc_sender_id()
	for child in list.get_children():
		if child.id == id:
			child.set_ready(pressed)
	try_enable_begin()

remote func assign_host_element(element):
	var id = get_tree().get_rpc_sender_id()
	assign_peer_element(id, element)

remote func assign_element(element):
	MpMan.local_player.element = element

remote func assign_peer_element(peer_id, element):
	for peer in MpMan.peers.values():
		if peer.id == peer_id:
			peer.element = element

remotesync func goto_next_scene():
	#TODO: Send to actual game
	get_tree().change_scene("res://scenes/Test.tscn")

func on_begin_pressed():
	if not MpMan.is_host():
		return
	
	randomize()
	
	# Assign elements
	var local_elem = randi() % 8
	MpMan.local_player.element = unassigned_elements[local_elem]
	unassigned_elements.remove(local_elem)
	rpc("assign_host_element", MpMan.local_player.element) # Jank
	
	for peer in MpMan.peers.values():
		var elem = randi() % unassigned_elements.size()
		peer.element = unassigned_elements[elem]
		rpc_id(peer.id, "assign_element", peer.element)
		rpc("assign_peer_element", peer.id, peer.element)
		unassigned_elements.remove(elem)
	
	MpMan.disallow_new_connections()
	rpc("set_seed_for_all", randi())
	rpc("goto_next_scene")

remotesync func set_seed_for_all(new_seed):
	seed(new_seed)

func on_lockins_changed(id):
	for child in list.get_children():
		if child.id == id:
			child.set_ready(true)
	try_enable_begin()
