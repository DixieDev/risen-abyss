extends Control

signal lockins_changed(id)

onready var list = get_node("CharacterList")
onready var lockin = get_node("Lock-In")
onready var info = get_node("Info")
onready var info_name = get_node("Info/Name")
onready var info_icon = get_node("Info/BigIcon")
onready var info_desc = get_node("Info/Description")

var current_selection = "None"

onready var index_to_char = [
	"Dog",
	"Snake",
	"Ant",
	"Tree",
]

func _ready():
	lockin.visible = false
	lockin.disabled = true
	info.visible = false
	info_name.bbcode_enabled = true
	for character in index_to_char:
		list.add_icon_item(GameConstants.characters[character].texture, true)
	list.select(0)
	on_character_selected(0)


func on_character_selected(index):
	if list.is_item_disabled(index):
		return
	
	var char_name = index_to_char[index]
	info_name.bbcode_text = "[center]" + char_name
	info_icon.texture = GameConstants.characters[char_name].texture
	info_desc.text = GameConstants.characters[char_name].description
	info.visible = true
	current_selection = char_name
	lockin.visible = true
	lockin.disabled = false

func on_lockin_pressed():
	for i in range(list.get_item_count()):
		list.set_item_disabled(i, true)
	lockin.disabled = true
	rpc("register_selection", current_selection)
	MpMan.local_player.character = current_selection
	emit_signal("lockins_changed", -1)

remote func register_selection(character):
	var id = get_tree().get_rpc_sender_id()
	MpMan.peers[id].character = character
	emit_signal("lockins_changed", id)

func get_locked_in_count():
	var decisive_players = 0
	for peer in MpMan.peers.values():
		if peer.character != "None":
			decisive_players += 1
	
	if MpMan.local_player.character != "None":
		decisive_players += 1
	
	return decisive_players
