extends CanvasLayer

var return_to_mm = false

func _ready():
# warning-ignore:return_value_discarded
	MpMan.connect("server_disconnected", self, "on_disconnected_from_server")
# warning-ignore:return_value_discarded
	MpMan.connect("peer_disconnected", self, "on_peer_disconnected")
	
	if OS.has_feature("editor"):
		return
	$ErrorPopup.dialog_text = """Welcome to Risen Abyss. Each turn, your chosen Fallen can perform up to two actions, such as 
	moving, playing a card, or using equipment. Everything that affects the game world goes through 
	left-click. For example, to move your character, drag and drop them to the place you want, or 
	to use an equipment simply left-click its icon.
	
	To move the camera, you can either click the middle-mouse button and drag, or use WASD. For 
	more information, you can right-click anything in the world or UI to get a popup with more 
	details (not actually in the game yet sorry!)"""
	$ErrorPopup.popup_centered()

# warning-ignore:unused_argument
func on_peer_disconnected(id):
	$ErrorPopup.dialog_text = "A player disconnected, the game will continue without them (doesn't actually work right now)"
	$ErrorPopup.popup_centered()
	# TODO: Delete missing peer off the face of the world?

func on_disconnected_from_server():
	$ErrorPopup.dialog_text = "Lost connection to host; returning to main menu"
	$ErrorPopup.popup_centered()
	return_to_mm = true

func on_error_popup_hidden():
	if return_to_mm:
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://scenes/MainMenu.tscn")

func confirm_empty_turn():
	$EmptyTurnPopup.popup_centered()
