extends Camera2D

const speed = 720.0
const max_range = 350.0

var target: Character
var held = false
var following = true

func assign_target(new_target: Character):
	target = new_target

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == 3:
			if event.pressed:
				grab()
			else:
				release()
	elif held and event is InputEventMouseMotion:
		position -= event.relative

func grab():
	held = true
	following = false

func release():
	held = false

func follow_player():
	following = true
	held = false

func _process(delta):
	if following:
		position = target.position

	if not held:
		var axes = Vector2.ZERO
		if Input.is_key_pressed(KEY_W):axes.y -= 1
		if Input.is_key_pressed(KEY_A): axes.x -= 1
		if Input.is_key_pressed(KEY_S): axes.y += 1
		if Input.is_key_pressed(KEY_D): axes.x += 1
		
		if axes.length_squared() > 0:
			following = false
	
		var velocity = axes.normalized() * speed
		position += velocity * delta
	clamp_position()

func clamp_position():
	var displacement = position - target.position
	if (displacement).length_squared() > max_range*max_range:
		position = target.position + displacement.normalized() * max_range
