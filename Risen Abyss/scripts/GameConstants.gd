extends Node2D

onready var characters = {
	"Dog": {
		"frame": 0,
		"texture": load("res://art/characters/dog.tres"),
		"description": "The Fallen best suited for beginners; the Dog is a fast and powerful melee fighter with a simple gameplan.\n\nStarting card: Bite"
	},
	"Snake": {
		"frame": 1,
		"texture": load("res://art/characters/snake.tres"),
		"description": "Neither the easiest nor hardest of the Fallen to play; the Snake is known to immobilize or poison her targets before facing them head on for an assured victory.\n\nStarting card: Poisonous Bite"
	},
	"Ant": {
		"frame": 2,
		"texture": load("res://art/characters/ant.tres"),
		"description": "An advanced Fallen; the Ant lifts his enemies overhead before throwing, pummelling, or crushing them.\n\nStarting card: Lift/Throw"
	},
	"Tree": {
		"frame": 3,
		"texture": load("res://art/characters/tree.tres"),
		"description": "An intermediate Fallen; the Tree protects her forest from intruders by cooperating with the natural world.\n\nStarting card: Forage"
	}
}

var elements = {
	"Creation": {
		"colour_code": "#ffff00",
	},
	"Void": {
		"colour_code": "#7700aa",
	},
	"Light": {
		"colour_code": "#ffffff",
	},
	"Darkness": {
		"colour_code": "#999999",
	},
	"Fire": {
		"colour_code": "#ff0000",
	},
	"Water": {
		"colour_code": "#4444ff",
	},
	"Earth": {
		"colour_code": "#dd6600",
	},
	"Air": {
		"colour_code": "#00ff00",
	}
}

func make_element_info(name, colour_code):
	return {
		"name": name,
		"colour_code": colour_code,
	}
