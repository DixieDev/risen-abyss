extends Node2D

signal submitted_turn

var controllable = true
var pawn: Character
var holding_pawn = false
var hovering_pawn = false

onready var world = get_node("../../GameWorld")
onready var ui = get_node("../UI")
onready var bottom_bar = get_node("../UI/BottomBar")
onready var arrow = load("res://art/arrows/arrow.tres")
onready var straight = load("res://art/arrows/straight.tres")
onready var bend = load("res://art/arrows/bend.tres")

const cell_size = 64
const half_cell = cell_size/2
var turn_start_pos
var movement = 4
var steps = []
var permitted_steps = [
	Vector2(cell_size, 0),
	Vector2(-cell_size, 0),
	Vector2(0, cell_size),
	Vector2(0, -cell_size),
]

var max_action_points = 2
var action_points = max_action_points
var selected_actions = []
var submitted_turn = false

func assign_pawn(new_pawn):
	pawn = new_pawn
	turn_start_pos = pawn.position

func enable_control():
	controllable = true

func disable_control():
	controllable = false
	holding_pawn = false

func _ready():
	Game.connect("on_all_turns_complete", self, "on_all_turns_complete")

func _input(event):
	if controllable:
		if event is InputEventMouseButton and event.button_index == 1:
			if event.pressed and (action_points > 0 or steps.size() > 0) and pawn.is_mouse_hovered():
				holding_pawn = true
				steps.clear()
			elif holding_pawn and not event.pressed:
				holding_pawn = false
		elif event is InputEventKey and action_points > 0:
			pass

func _process(_delta):
	if holding_pawn:
		process_drag()
	
	var action_info = []
	action_points = max_action_points
	if steps.size() > 0: 
		action_info.append("move")
		action_points -= 1
	for action in selected_actions:
		action_info.append(action)
	action_points -= selected_actions.size()
	
	bottom_bar.set_action_info(action_info, action_points == 0)
	
	update()

func process_drag():
	while true:
		var init_pos = apply_steps(turn_start_pos)
		var disp = get_global_mouse_position() - init_pos
		if disp.length_squared() > half_cell*half_cell:
			var best = permitted_steps[0]
			var best_dot = 0
			for possible in permitted_steps:
				var dot = disp.normalized().dot(possible.normalized())
				if dot >= best_dot:
					best_dot = dot
					best = possible
			
			# Determine which way counts as 'backwards'
			var backwards = Vector2.ZERO
			if steps.size() > 0:
				backwards = -steps[steps.size()-1]
			
			if best == backwards:
				# If our best option is to go backwards, pop the latest step
				steps.pop_back()
				break
			elif steps.size() == movement:
				# Break if we can't add more steps
				break
			elif not world.is_position_free(init_pos + best):
				# Break if we can't step into the best cell
				break
			else:
				# Otherwise we're good to add our best step
				steps.append(best)
		else:
			break

func apply_steps(start: Vector2) -> Vector2:
	var dest = start
	for step in steps:
		dest += step
	return dest

func angle_for_step(step: Vector2) -> float:
	if step == permitted_steps[0]: return PI
	elif step == permitted_steps[1]: return 0.0
	elif step == permitted_steps[2]: return PI * 1.5
	else: return PI / 2.0

func _draw():
	draw_steps()
	if holding_pawn:
		draw_ghost(get_global_mouse_position())
	elif steps.size() > 0:
		draw_ghost(apply_steps(turn_start_pos))

func draw_ghost(pos):
	var src: Rect2 = pawn.character_texture.region
	var dest = Rect2(pos - src.size/2, src.size)
	var modulation = pawn.modulate
	modulation.a = 0.5
	draw_texture_rect_region(pawn.texture, dest, src, modulation)

func draw_steps():
	var current_pos = turn_start_pos
	for i in range(steps.size()):
		draw_set_transform(Vector2.ZERO, 0.0, Vector2.ONE)
		var step: Vector2 = steps[i]
		current_pos += step
		if i<steps.size()-1:
			var next = steps[i+1]
			if next == step:
				# If the current step is the same as the next, draw a straight
				draw_set_transform(current_pos, angle_for_step(step), Vector2.ONE)
				draw_texture(straight, Vector2.ZERO)
			else:
				# Else draw a bend. Getting the angle right is a pain
				var rot = angle_for_step(step)
				var next_rot = angle_for_step(next)
				if rot == 0 and next_rot == PI*1.5:
					pass
				elif rot == PI*1.5 and next_rot == 0:
					rot -= PI/2
				elif next_rot > rot:
					rot -= PI/2
				draw_set_transform(current_pos, rot, Vector2.ONE)
				draw_texture(bend, Vector2.ZERO)
		else:
			# Draw the arrow head
			draw_set_transform(current_pos, angle_for_step(step), Vector2.ONE)
			draw_texture(arrow, Vector2.ZERO)
	draw_set_transform(Vector2.ZERO, 0.0, Vector2.ONE)

func on_act_pressed():
	if action_points == max_action_points:
		ui.confirm_empty_turn()
	else:
		submit_turn()

func on_empty_turn_confirmed():
	submit_turn()

func submit_turn():
	if not submitted_turn:
		disable_control()
		submitted_turn = true
		emit_signal("submitted_turn")
		register_local_actions()
		rpc("register_peer_actions", steps, selected_actions)
	else:
		printerr("Attempted to submit turn twice")

func register_local_actions():
	Game.set_local_actions(steps, selected_actions)

remote func register_peer_actions(peer_steps, peer_actions):
	var id = get_tree().get_rpc_sender_id()
	Game.add_peer_actions(id, peer_steps, peer_actions)

func on_all_turns_complete():
	enable_control()
	submitted_turn = false
	steps.clear()
	selected_actions.clear()
	turn_start_pos = pawn.position

func on_rest_pressed():
	if action_points != 0:
		selected_actions.append("rest")

func on_defend_pressed():
	if action_points != 0:
		selected_actions.append("defend")

func on_interact_pressed():
	if action_points != 0:
		selected_actions.append("interact")

